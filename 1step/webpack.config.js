var ExtractTextPlugin = require('./extract-text-webpack-plugin');
module.exports = {
    //файлик откуда будет всё собираться
    entry: "./main.js",
    //файлик куда всё это будет собираться
    output: {
        filename: "bundle.js"
    },
    //отображаем папку откуда будут загружаться модули, которые можно импортировать в переменную и использовать
    resolse: {
        modulesDirectories: ['node_modules']
    },
    //список модулей подгрухающихся сразу, то, что одлжно работать сразу
    module: {
        loaders: [
        {
            test: /\.js/,
            loader: 'babel',
            exlude: /(node_modules|bower_components)/
        },
        {
            test: /\.css/,
            loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
        },

        ]
    },
    //плагины
    plugins: [
        new ExtractTextPlugin('bundle.css')
    ]

};