const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        index: './js/index.js',
        task: './js/task.js',
        admin: './js/admin.js'
    },
    output: {
        //в исходном файле папка для пути
        path: path.resolve(__dirname, './dist'),
        filename: 'js/[name].bundle.js',
        publicPath: '/assets'
    },
    //берет индекс из указаной папки и грузит его сервер
    //node_modules/.bin/webpack-dev-server
    //для переписны изменений для работы из file:// надо пересобрать
    devServer: {
        contentBase: path.resolve(__dirname),
        port: 8090,
        hot: true
    },
    module: {
        rules: 
        [
            //JS
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015']
                    }
                }]
            },
            //import './index.css'; в файле (путь относительно js) CSS
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({ 
                fallback: 'style-loader', 
                use: ['css-loader', 'postcss-loader']})
            },
            //import в js файле не забыть SASS (требует node-sass)
            {
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            },
            //import в js файле не забыть (требует less) LESS
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'less-loader']
                })
            }
        ]
    },
    plugins: [
    //в какой файл будет генериться
    new ExtractTextPlugin("css/[name].bundle.css"),
    //autoprefixer вместе с postcss-loader используется
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          require('autoprefixer')({
            browsers: ['last 4 versions'],
            cascade: false
          })
        ]
      }
    }),
    //для сжатия кода там и по мелочи
    new UglifyJSPlugin({
        options: {
            beautify: true
        },
            comments: true,
            compress: {
                warnings: false,
                drop_console: true
            }
    }),
    //для переноса из папки разработки в dist, путь строится относительно контекста
    new CopyWebpackPlugin ([
        {from: 'fonts', to: 'fonts'},
        {from: 'img', to: 'img'},
        {from: 'icons', to: 'icons'}
    ]),
    //для обновления страницы при обновлении модулей(js)
    new webpack.HotModuleReplacementPlugin()
]
}